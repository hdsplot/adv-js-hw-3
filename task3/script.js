const user1 = {
    name: "John",
    years: 30
};

const { name, years, isAdmin = false } = user1;

const div = document.createElement("div")
document.body.append(div);

div.insertAdjacentHTML("beforeend",
    `<ul>
<li>Name: ${user1.name}</li>
<li>Age: ${user1.years}</li>
<li>isAdmin: ${userIsAdmin(user1)}</li>
</ul>`)

function userIsAdmin(obj) {
    if (!obj.hasOwnProperty("isAdmin")) {
        return isAdmin;
    }
    return obj.isAdmin;
}
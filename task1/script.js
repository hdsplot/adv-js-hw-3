// Задача 1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

function uniteArr(arr1, arr2) {
  return [...new Set([...arr1, ...arr2])];
}
const generalClients = uniteArr(clients1, clients2);
console.log(generalClients);
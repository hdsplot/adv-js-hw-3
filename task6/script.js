const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = { ...employee, age: 51, salary: '$3000' };
console.log(newEmployee);